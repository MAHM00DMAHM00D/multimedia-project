﻿namespace Multimedia;

public enum Operation
{
    Blending,
    Cropping,
    AddingText,
    AddingAudio,
    Sharpening
}
