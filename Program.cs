using Multimedia.Forms;
using QuestPDF.Infrastructure;

namespace Multimedia
{
    internal static class Program
    {
        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            QuestPDF.Settings.License = LicenseType.Community;
            ApplicationConfiguration.Initialize();
            Application.Run(new MainForm());
        }
    }
}
