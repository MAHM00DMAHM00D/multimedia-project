﻿using System.Data;

namespace Multimedia.Forms
{
    public partial class SearchForm : Form
    {
        private static readonly string[] _sourceArray = [".jpg", ".png", ".bmp"];
        private readonly Action<string> _onLoadSelectedImageClick;

        public SearchForm(Action<string> onLoadSelectedImageClick)
        {
            InitializeComponent();
            _onLoadSelectedImageClick = onLoadSelectedImageClick;
        }

        private async void OnSearchButtonClick(object sender, EventArgs e)
        {
            listView.Items.Clear();
            progressBar.Value = 0;
            button1.Enabled = false;
            await Task.Run(() =>
            {
                List<ListItem> items;
                imageList.Images.Clear();
                if (widthHeightRadioButton.Checked)
                {
                    items = SearchForImagesByWidthAndHeight(
                        selectedFolderTextBox.Text,
                        (int)widthUpDown.Value,
                        (int)heightUpDown.Value
                    );
                }
                else
                {
                    items = SearchForImagesByLastWriteDate(selectedFolderTextBox.Text);
                }

                if (items.Count == 0)
                {
                    loadSelectedButton.Invoke(() => loadSelectedButton.Enabled = false);
                    MessageBox.Show("No Images Found");
                    return;
                }

                loadSelectedButton.Invoke(() => loadSelectedButton.Enabled = true);

                for (int i = 0; i < items.Count; i++)
                {
                    var item = new ListViewItem(items[i].Name, i);
                    item.SubItems.Add(items[i].Width.ToString());
                    item.SubItems.Add(items[i].Height.ToString());
                    item.SubItems.Add(items[i].CreationTime.ToString("yyyy-MM-dd"));
                    item.SubItems.Add(items[i].LastWriteTime.ToString("yyyy-MM-dd"));
                    AddItem(item);
                }
            });
            progressBar.Value = 0;
            button1.Enabled = true;
        }

        private List<ListItem> SearchForImagesByLastWriteDate(string folderPath)
        {
            List<ListItem> list = [];
            string[] imageFiles = Directory
                .GetFiles(folderPath, ".", SearchOption.TopDirectoryOnly)
                .Where(f => _sourceArray.Contains(Path.GetExtension(f).ToLower()))
                .Where(f => File.GetLastWriteTime(f).Date == dateTimePicker.Value.Date)
                .ToArray();
            SetProgressMaxValue(imageFiles.Length);
            int i = 1;
            foreach (string filePath in imageFiles)
            {
                using Image image = Image.FromFile(filePath);
                int width = image.Width;
                int height = image.Height;
                list.Add(
                    new ListItem(
                        Path.GetFileName(filePath),
                        width,
                        height,
                        File.GetCreationTime(filePath),
                        File.GetLastWriteTime(filePath)
                    )
                );
                UpdateProgress(i++);
                imageList.Images.Add(image);
            }

            return list;
        }

        private List<ListItem> SearchForImagesByWidthAndHeight(
            string folderPath,
            int desiredWidth,
            int desiredHeight
        )
        {
            List<ListItem> list = [];
            string[] imageFiles = Directory
                .GetFiles(folderPath, ".", SearchOption.TopDirectoryOnly)
                .Where(f => _sourceArray.Contains(Path.GetExtension(f).ToLower()))
                .ToArray();
            SetProgressMaxValue(imageFiles.Length);
            int i = 1;
            foreach (string filePath in imageFiles)
            {
                using Image image = Image.FromFile(filePath);
                int width = image.Width;
                int height = image.Height;
                if (width == desiredWidth && height == desiredHeight)
                {
                    list.Add(
                        new ListItem(
                            Path.GetFileName(filePath),
                            width,
                            height,
                            File.GetCreationTime(filePath),
                            File.GetLastWriteTime(filePath)
                        )
                    );
                    imageList.Images.Add(image);
                }
                UpdateProgress(i++);
            }

            return list;
        }

        private void OnSelectFolderButtonClick(object sender, EventArgs e)
        {
            FolderBrowserDialog dialog = new();

            if (dialog.ShowDialog() != DialogResult.OK)
            {
                return;
            }

            selectedFolderTextBox.Text = dialog.SelectedPath;
            listView.Items.Clear();
            imageList.Images.Clear();

            button1.Enabled = true;
            dateRadioButton.Enabled = true;
            widthHeightRadioButton.Enabled = true;
            dateTimePicker.Enabled = true;
            widthUpDown.Enabled = true;
            heightUpDown.Enabled = true;
        }

        private void OnListViewComboBoxSelectedIndexChanged(object sender, EventArgs e)
        {
            Enum.TryParse(listViewComboBox.Text, out View view);
            listView.View = view;
        }

        private void OnLoadSelectedButtonClick(object sender, EventArgs e)
        {
            if (listView.SelectedItems.Count == 0)
            {
                return;
            }

            _onLoadSelectedImageClick(
                Path.Combine(selectedFolderTextBox.Text, listView.SelectedItems[0].Text)
            );
        }

        private void SetProgressMaxValue(int value)
        {
            if (listView.InvokeRequired)
            {
                progressBar.Invoke(() => progressBar.Maximum = value);
            }
            else
            {
                progressBar.Maximum = value;
            }
        }

        private void UpdateProgress(int value)
        {
            if (listView.InvokeRequired)
            {
                progressBar.Invoke(() => progressBar.Value = value);
            }
            else
            {
                progressBar.Value = value;
            }
        }

        private void AddItem(ListViewItem item)
        {
            if (listView.InvokeRequired)
            {
                listView.Invoke(() => listView.Items.Add(item));
                listView.Invalidate();
            }
            else
            {
                listView.Items.Add(item);
            }
        }
    }

    public record ListItem(
        string Name,
        int Width,
        int Height,
        DateTime CreationTime,
        DateTime LastWriteTime
    );
}
