﻿using System.Drawing.Imaging;
using System.IO.Compression;
using System.Windows.Forms;
using Multimedia.Helpers;
using QuestPDF.Fluent;

namespace Multimedia.Forms
{
    public partial class ExportZipForm : Form
    {
        private readonly Bitmap _image;
        private readonly string _progress;
        private readonly string _classification;

        public ExportZipForm(Bitmap image, string progress, string classification)
        {
            InitializeComponent();
            _image = image;
            _progress = progress;
            _classification = classification;
        }

        private void OnIncludePDFCheckBoxCheckedChanged(object sender, EventArgs e)
        {
            detailsRichTextBox.Enabled = includePDFCheckBox.Checked;
            nameTextBox.Enabled = includePDFCheckBox.Checked;
        }

        private void OnIncludeAudioCheckBoxCheckedChanged(object sender, EventArgs e)
        {
            selectAudioButton.Enabled = includeAudioCheckBox.Checked;
        }

        private async void OnExportZipButtonClick(object sender, EventArgs e)
        {
            if (includeAudioCheckBox.Checked && string.IsNullOrEmpty(audioFilePathTextBox.Text))
            {
                MessageBox.Show("Select an audio first");
                return;
            }

            if (includeAudioCheckBox.Checked && !File.Exists(audioFilePathTextBox.Text))
            {
                MessageBox.Show("Audio does not exists");
                return;
            }

            string zipFilePath = Path.ChangeExtension(Path.GetTempFileName(), ".zip");
            bool share = ((Button)sender).Name == "shareZipButton";
            if (!share)
            {
                SaveFileDialog saveFileDialog = new() { Filter = "Zip|*.zip", AddExtension = true };

                if (saveFileDialog.ShowDialog() != DialogResult.OK)
                {
                    return;
                }

                zipFilePath = saveFileDialog.FileName;
            }

            try
            {
                shareZipButton.Enabled = false;
                exportZipButton.Enabled = false;

                string extension;
                ImageFormat format;
                if (nameof(ImageFormat.Bmp) == imageFormatComboBox.Text)
                {
                    extension = ".bmp";
                    format = ImageFormat.Bmp;
                }
                else if (nameof(ImageFormat.Png) == imageFormatComboBox.Text)
                {
                    extension = ".png";
                    format = ImageFormat.Png;
                }
                else
                {
                    extension = ".jpg";
                    format = ImageFormat.Jpeg;
                }

                _ = Enum.TryParse(
                    compressionLevelComboBox.Text,
                    out CompressionLevel compressionLevel
                );

                using FileStream zipToOpen = new(zipFilePath, FileMode.Create);
                using ZipArchive archive = new(zipToOpen, ZipArchiveMode.Create);

                var tempImagePath = Path.GetTempFileName();
                _image.Save(tempImagePath, format);
                ZipArchiveEntry imageEntry = archive.CreateEntry(
                    $"Image.{extension}",
                    compressionLevel
                );
                using Stream imageEntryStream = imageEntry.Open();
                using FileStream imageFileStream = new(tempImagePath, FileMode.Open);
                imageFileStream.CopyTo(imageEntryStream);
                imageEntryStream.Close();

                if (includePDFCheckBox.Checked)
                {
                    var document = PDFGenerator.GeneratePDFReport(
                        _image,
                        nameTextBox.Text,
                        _progress,
                        _classification,
                        detailsRichTextBox.Text
                    );

                    var tempPDFPath = Path.GetTempFileName();
                    document.GeneratePdf(tempPDFPath);
                    ZipArchiveEntry pdfEntry = archive.CreateEntry("Report.pdf", compressionLevel);
                    using Stream entryStream = pdfEntry.Open();
                    using FileStream sourceFileStream = new(tempPDFPath, FileMode.Open);
                    sourceFileStream.CopyTo(entryStream);
                    entryStream.Close();
                }

                if (includeAudioCheckBox.Checked)
                {
                    ZipArchiveEntry pdfEntry = archive.CreateEntry("Audio.wav", compressionLevel);
                    using Stream entryStream = pdfEntry.Open();
                    using FileStream sourceFileStream =
                        new(audioFilePathTextBox.Text, FileMode.Open);
                    sourceFileStream.CopyTo(entryStream);
                    entryStream.Close();
                }

                zipToOpen.Close();

                if (share)
                {
                    try
                    {
                        var shareResult = await ShareHelper.ShareZipFile(zipFilePath);
                        if (!string.IsNullOrEmpty(shareResult?.Link))
                        {
                            ShareHelper.OpenFileUsingDefaultProgram(
                                ShareHelper.WhatsappUrl(
                                    $"Here is your report file {shareResult.Link} you can access it before the {shareResult.Expires:yyyy-MM-dd HH:mm}"
                                )
                            );
                        }
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Something went wrong, check your Internet connection");
                    }
                }

                shareZipButton.Enabled = true;
                exportZipButton.Enabled = true;
            }
            catch (Exception) { }

            shareZipButton.Enabled = true;
            exportZipButton.Enabled = true;
        }

        private void OnSelectAudioButtonClick(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog =
                new()
                {
                    Filter = "Wave File|*.wav",
                    CheckPathExists = true,
                    AddExtension = true
                };

            if (openFileDialog.ShowDialog() != DialogResult.OK)
            {
                return;
            }

            audioFilePathTextBox.Text = openFileDialog.FileName;
        }
    }
}
