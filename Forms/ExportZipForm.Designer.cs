﻿namespace Multimedia.Forms
{
    partial class ExportZipForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            exportZipButton = new Button();
            includePDFCheckBox = new CheckBox();
            compressionLevelComboBox = new ComboBox();
            label6 = new Label();
            includeAudioCheckBox = new CheckBox();
            detailsRichTextBox = new RichTextBox();
            nameTextBox = new TextBox();
            label5 = new Label();
            label4 = new Label();
            audioFilePathTextBox = new TextBox();
            selectAudioButton = new Button();
            label1 = new Label();
            imageFormatComboBox = new ComboBox();
            shareZipButton = new Button();
            SuspendLayout();
            // 
            // exportZipButton
            // 
            exportZipButton.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            exportZipButton.Location = new Point(321, 453);
            exportZipButton.Name = "exportZipButton";
            exportZipButton.Size = new Size(95, 23);
            exportZipButton.TabIndex = 0;
            exportZipButton.Text = "Export Zip";
            exportZipButton.UseVisualStyleBackColor = true;
            exportZipButton.Click += OnExportZipButtonClick;
            // 
            // includePDFCheckBox
            // 
            includePDFCheckBox.AutoSize = true;
            includePDFCheckBox.Location = new Point(12, 12);
            includePDFCheckBox.Name = "includePDFCheckBox";
            includePDFCheckBox.Size = new Size(127, 19);
            includePDFCheckBox.TabIndex = 1;
            includePDFCheckBox.Text = "Include PDF Report";
            includePDFCheckBox.UseVisualStyleBackColor = true;
            includePDFCheckBox.CheckedChanged += OnIncludePDFCheckBoxCheckedChanged;
            // 
            // compressionLevelComboBox
            // 
            compressionLevelComboBox.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            compressionLevelComboBox.FormattingEnabled = true;
            compressionLevelComboBox.Items.AddRange(new object[] { "Optimal", "Fastest", "NoCompression", "SmallestSize" });
            compressionLevelComboBox.Location = new Point(12, 453);
            compressionLevelComboBox.Name = "compressionLevelComboBox";
            compressionLevelComboBox.Size = new Size(104, 23);
            compressionLevelComboBox.TabIndex = 7;
            compressionLevelComboBox.Text = "Optimal";
            // 
            // label6
            // 
            label6.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            label6.AutoSize = true;
            label6.Font = new Font("Segoe UI", 9F, FontStyle.Bold);
            label6.Location = new Point(12, 432);
            label6.Margin = new Padding(3, 12, 3, 3);
            label6.Name = "label6";
            label6.Size = new Size(111, 15);
            label6.TabIndex = 6;
            label6.Text = "Compression Level";
            // 
            // includeAudioCheckBox
            // 
            includeAudioCheckBox.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            includeAudioCheckBox.AutoSize = true;
            includeAudioCheckBox.Location = new Point(12, 310);
            includeAudioCheckBox.Name = "includeAudioCheckBox";
            includeAudioCheckBox.Size = new Size(100, 19);
            includeAudioCheckBox.TabIndex = 1;
            includeAudioCheckBox.Text = "Include Audio";
            includeAudioCheckBox.UseVisualStyleBackColor = true;
            includeAudioCheckBox.CheckedChanged += OnIncludeAudioCheckBoxCheckedChanged;
            // 
            // detailsRichTextBox
            // 
            detailsRichTextBox.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
            detailsRichTextBox.Enabled = false;
            detailsRichTextBox.Location = new Point(12, 103);
            detailsRichTextBox.Name = "detailsRichTextBox";
            detailsRichTextBox.Size = new Size(404, 201);
            detailsRichTextBox.TabIndex = 11;
            detailsRichTextBox.Text = "";
            // 
            // nameTextBox
            // 
            nameTextBox.Enabled = false;
            nameTextBox.Location = new Point(100, 40);
            nameTextBox.Name = "nameTextBox";
            nameTextBox.Size = new Size(315, 23);
            nameTextBox.TabIndex = 10;
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Font = new Font("Segoe UI", 9F, FontStyle.Bold);
            label5.Location = new Point(12, 73);
            label5.Margin = new Padding(3, 3, 3, 12);
            label5.Name = "label5";
            label5.Size = new Size(81, 15);
            label5.TabIndex = 8;
            label5.Text = "More Details:";
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Font = new Font("Segoe UI", 9F, FontStyle.Bold);
            label4.Location = new Point(12, 43);
            label4.Margin = new Padding(3, 3, 3, 12);
            label4.Name = "label4";
            label4.Size = new Size(86, 15);
            label4.TabIndex = 9;
            label4.Text = "Patient Name:";
            // 
            // audioFilePathTextBox
            // 
            audioFilePathTextBox.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
            audioFilePathTextBox.Location = new Point(12, 335);
            audioFilePathTextBox.Name = "audioFilePathTextBox";
            audioFilePathTextBox.ReadOnly = true;
            audioFilePathTextBox.Size = new Size(303, 23);
            audioFilePathTextBox.TabIndex = 10;
            // 
            // selectAudioButton
            // 
            selectAudioButton.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            selectAudioButton.Enabled = false;
            selectAudioButton.Location = new Point(321, 334);
            selectAudioButton.Name = "selectAudioButton";
            selectAudioButton.Size = new Size(95, 23);
            selectAudioButton.TabIndex = 0;
            selectAudioButton.Text = "Select Audio";
            selectAudioButton.UseVisualStyleBackColor = true;
            selectAudioButton.Click += OnSelectAudioButtonClick;
            // 
            // label1
            // 
            label1.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            label1.AutoSize = true;
            label1.Font = new Font("Segoe UI", 9F, FontStyle.Bold);
            label1.Location = new Point(13, 373);
            label1.Margin = new Padding(3, 12, 3, 3);
            label1.Name = "label1";
            label1.Size = new Size(85, 15);
            label1.TabIndex = 6;
            label1.Text = "Image Format";
            // 
            // imageFormatComboBox
            // 
            imageFormatComboBox.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            imageFormatComboBox.FormattingEnabled = true;
            imageFormatComboBox.Items.AddRange(new object[] { "Jpeg", "Png", "Bmp" });
            imageFormatComboBox.Location = new Point(13, 394);
            imageFormatComboBox.Name = "imageFormatComboBox";
            imageFormatComboBox.Size = new Size(104, 23);
            imageFormatComboBox.TabIndex = 7;
            imageFormatComboBox.Text = "Jpeg";
            // 
            // shareZipButton
            // 
            shareZipButton.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            shareZipButton.Location = new Point(220, 453);
            shareZipButton.Name = "shareZipButton";
            shareZipButton.Size = new Size(95, 23);
            shareZipButton.TabIndex = 0;
            shareZipButton.Text = "Share Zip";
            shareZipButton.UseVisualStyleBackColor = true;
            shareZipButton.Click += OnExportZipButtonClick;
            // 
            // ExportZipForm
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(425, 488);
            Controls.Add(detailsRichTextBox);
            Controls.Add(audioFilePathTextBox);
            Controls.Add(nameTextBox);
            Controls.Add(label5);
            Controls.Add(label4);
            Controls.Add(imageFormatComboBox);
            Controls.Add(compressionLevelComboBox);
            Controls.Add(label1);
            Controls.Add(label6);
            Controls.Add(includeAudioCheckBox);
            Controls.Add(includePDFCheckBox);
            Controls.Add(selectAudioButton);
            Controls.Add(shareZipButton);
            Controls.Add(exportZipButton);
            MinimumSize = new Size(441, 527);
            Name = "ExportZipForm";
            Text = "ExportZipForm";
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Button exportZipButton;
        private CheckBox includePDFCheckBox;
        private ComboBox compressionLevelComboBox;
        private Label label6;
        private CheckBox includeAudioCheckBox;
        private RichTextBox detailsRichTextBox;
        private TextBox nameTextBox;
        private Label label5;
        private Label label4;
        private TextBox audioFilePathTextBox;
        private Button selectAudioButton;
        private Label label1;
        private ComboBox imageFormatComboBox;
        private Button shareZipButton;
    }
}