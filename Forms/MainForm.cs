using System.Drawing.Imaging;
using Multimedia.Controllers;
using Multimedia.Helpers;

namespace Multimedia.Forms
{
    public partial class MainForm : Form
    {
        private bool _seachFormOpend = false;
        private Operation? _currentOperation;

        private string _progress = "None";

        private SelectionMode _selectionMode = SelectionMode.Rectangle;

        // Represent the history of modification
        // The first bitmap is the original image
        private readonly Stack<Bitmap> _bitmaps = [];

        private readonly List<Color[]> _colorMaps = [];

        private bool _isMouseDown;

        private readonly List<Point> _points = [];
        private Rectangle _selectionRectangle;

        private Action<Point>? _onSelectTextPositionByMouse;

        public MainForm()
        {
            InitializeComponent();
            ColorMaps.AddColorMaps(_colorMaps);
        }

        private void OpenToolStripMenuItemClick(object sender, EventArgs e)
        {
            if (_bitmaps.Count > 1 && !DiscardChanges())
            {
                return;
            }

            var openImageDialog = new OpenFileDialog
            {
                CheckFileExists = true,
                Filter = "JPEG|*.jpg|PNG|*.png"
            };
            DialogResult result = openImageDialog.ShowDialog();

            if (result == DialogResult.OK)
            {
                _bitmaps.Clear();
                _bitmaps.Push(new Bitmap(openImageDialog.FileName));
                mainPictureBox.Image = _bitmaps.Peek();
            }
        }

        private void MainPictureBoxMouseDown(object sender, MouseEventArgs e)
        {
            if (_bitmaps.Count < 1)
            {
                return;
            }

            var originalImage = _bitmaps.Peek();
            if (e.Button == MouseButtons.Left)
            {
                if (_currentOperation == Operation.Cropping)
                {
                    _isMouseDown = true;
                    if (_selectionMode == SelectionMode.Rectangle)
                    {
                        if (_points.Count == 0)
                        {
                            _points.Add(e.Location);
                        }
                        else
                        {
                            _points[0] = e.Location;
                        }

                        _selectionRectangle = Rectangle.Empty;
                    }
                }
                if (_currentOperation == Operation.Blending)
                {
                    if (e.X > originalImage.Width)
                    {
                        return;
                    }

                    if (e.Y > originalImage.Height)
                    {
                        return;
                    }

                    _isMouseDown = true;
                    if (
                        _selectionMode == SelectionMode.Rectangle
                        || _selectionMode == SelectionMode.Ellipse
                    )
                    {
                        if (_points.Count == 0)
                        {
                            _points.Add(e.Location);
                        }
                        else
                        {
                            _points[0] = e.Location;
                        }

                        _selectionRectangle = Rectangle.Empty;
                    }
                    else if (_selectionMode == SelectionMode.Polygon)
                    {
                        _points.Add(e.Location);
                    }
                }
                else if (
                    _currentOperation == Operation.AddingText
                    && _onSelectTextPositionByMouse is not null
                )
                {
                    _onSelectTextPositionByMouse(e.Location);
                }
            }
            else if (e.Button == MouseButtons.Right)
            {
                if (_points.Count != 0)
                {
                    _points.RemoveAt(_points.Count - 1);
                }
            }
            mainPictureBox.Invalidate();
        }

        private void MainPictureBoxMouseMove(object sender, MouseEventArgs e)
        {
            if (_isMouseDown)
            {
                var originalImage = _bitmaps.Peek();
                var startPoint = _points[0];
                var startX = Math.Min(startPoint.X, e.X);
                var startY = Math.Min(startPoint.Y, e.Y);
                var width = Math.Abs(startPoint.X - e.X);
                var height = Math.Abs(startPoint.Y - e.Y);
                width = Math.Min(width, originalImage.Width);
                height = Math.Min(height, originalImage.Height);

                if (e.X < 0)
                {
                    startX = 0;
                }

                if (e.Y < 0)
                {
                    startY = 0;
                }

                if (e.X > originalImage.Width)
                {
                    width = Math.Abs(originalImage.Width - startX);
                }

                if (e.Y > originalImage.Height)
                {
                    height = Math.Abs(originalImage.Height - startY);
                }

                _selectionRectangle = new Rectangle(startX, startY, width, height);
                mainPictureBox.Invalidate(); // Trigger redrawing with selection
            }
        }

        private void MainPictureBoxMouseUp(object sender, MouseEventArgs e)
        {
            if (_isMouseDown)
            {
                _isMouseDown = false;
            }
        }

        private void MainPictureBoxPaint(object sender, PaintEventArgs e)
        {
            if (_bitmaps.Count < 1)
            {
                return;
            }

            Rectangle rectangle = _selectionRectangle;
            if (_selectionMode == SelectionMode.Rectangle)
            {
                e.Graphics.DrawRectangle(new Pen(Color.Red, 1.5F), rectangle);
            }
            else if (_selectionMode == SelectionMode.Polygon)
            {
                foreach (var point in _points)
                {
                    e.Graphics.FillEllipse(
                        new SolidBrush(Color.DarkRed),
                        new Rectangle
                        {
                            X = point.X - 2,
                            Y = point.Y - 2,
                            Width = 4,
                            Height = 4
                        }
                    );
                }

                if (_points.Count > 1)
                {
                    e.Graphics.DrawPolygon(new Pen(Color.Red, 1.5F), _points.ToArray());
                }
            }
            else if (_selectionMode == SelectionMode.Ellipse)
            {
                e.Graphics.DrawEllipse(new Pen(Color.Red, 1.5F), _selectionRectangle);
            }
        }

        private void SaveAsToolStripMenuItemClick(object sender, EventArgs e)
        {
            if (_bitmaps.Count == 0)
            {
                MessageBox.Show("No Image Selected");
                return;
            }

            SaveFileDialog saveFileDialog =
                new()
                {
                    CheckPathExists = true,
                    ValidateNames = true,
                    AddExtension = true,
                    Filter = "JPEG|*.jpg|PNG|*.png"
                };

            var result = saveFileDialog.ShowDialog();

            if (result != DialogResult.OK || string.IsNullOrWhiteSpace(saveFileDialog.FileName))
            {
                return;
            }

            mainPictureBox.Image.Save(
                saveFileDialog.FileName,
                saveFileDialog.FilterIndex == 0 ? ImageFormat.Jpeg : ImageFormat.Png
            );
        }

        private void UndoToolStripMenuItemClick(object sender, EventArgs e)
        {
            if (_bitmaps.Count == 0)
            {
                MessageBox.Show("No Image Selected");
                return;
            }

            if (1 < _bitmaps.Count)
            {
                _bitmaps.Pop();
                mainPictureBox.Image = _bitmaps.Peek();
            }
        }

        private void CompareToolStripMenuItemClick(object sender, EventArgs e)
        {
            if (_bitmaps.Count == 0)
            {
                MessageBox.Show("No Image Selected");
                return;
            }

            var form = new CompareForm(_bitmaps.Peek());
            form.FormClosed += (sender, e) =>
            {
                _progress = form.Progress;
                progressValueToolStripStatusLabel.Text = _progress;
            };

            form.ShowDialog();
        }

        private void OnClassificationToolStripMenuItemClick(object sender, EventArgs e)
        {
            if (_bitmaps.Count == 0)
            {
                MessageBox.Show("No Image Selected");
                return;
            }

            var menuItem = (ToolStripMenuItem)sender;
            classificationToolStripDropDownButton.Text = menuItem.Text;
        }

        private void OnSelectToBendToolStripMenuItemClick(object sender, EventArgs e)
        {
            if (_bitmaps.Count == 0)
            {
                MessageBox.Show("No Image Selected");
                return;
            }

            if (_currentOperation == Operation.Blending)
            {
                return;
            }

            _currentOperation = Operation.Blending;
            sidePanel.Controls.Clear();
            var controller = new BlendingController(OnApplyBending, OnSelectionModeChange)
            {
                Dock = DockStyle.Fill
            };
            sidePanel.Controls.Add(controller);
        }

        private void OnSelectionModeChange(SelectionMode selectionMode)
        {
            _selectionMode = selectionMode;
        }

        private void OnApplyBending(BendingArguments arguments)
        {
            if (_bitmaps.Count == 0)
            {
                MessageBox.Show("No Image Selected");
                return;
            }

            var originalImage = _bitmaps.Peek();
            Bitmap? modifiedImage;
            if (_selectionMode == SelectionMode.Rectangle)
            {
                if (_selectionRectangle.Width < 5 || _selectionRectangle.Height < 5)
                {
                    return;
                }

                modifiedImage = originalImage.ApplyColorMapOnRectangleSelection(
                    _selectionRectangle,
                    arguments.ColorMap
                );
            }
            else if (_selectionMode == SelectionMode.Polygon && _points.Count > 2)
            {
                modifiedImage = originalImage.ApplyColorMapOnPolygonSelection(
                    [.. _points],
                    arguments.ColorMap
                );
            }
            else
            {
                modifiedImage = originalImage.ApplyColorMapOnEllipseSelection(
                    _selectionRectangle,
                    arguments.ColorMap
                );
            }
            // Add item to the history
            if (modifiedImage != null)
                _bitmaps.Push(modifiedImage);
            // Show the modified image
            mainPictureBox.Image = _bitmaps.Peek();
        }

        private void OnAddTextToolStripMenuItemClick(object sender, EventArgs e)
        {
            if (_bitmaps.Count == 0)
            {
                MessageBox.Show("No Image Selected");
                return;
            }

            if (_currentOperation == Operation.AddingText)
            {
                return;
            }

            var image = _bitmaps.Peek();
            _currentOperation = Operation.AddingText;
            sidePanel.Controls.Clear();
            var controller = new AddTextController(
                image.Width,
                image.Height,
                ApplyText,
                PreviewText,
                PreviewText,
                PreviewText,
                PreviewText,
                PreviewText
            )
            {
                Dock = DockStyle.Fill
            };
            _onSelectTextPositionByMouse = controller.OnSelectTextPositionByMouse;
            sidePanel.Controls.Add(controller);
        }

        private void PreviewText(TextArguments arguments)
        {
            if (_bitmaps.Count == 0)
            {
                return;
            }

            if (!arguments.Preview)
            {
                return;
            }

            var image = _bitmaps.Peek();
            var modifiedImage = new Bitmap(image);

            if (modifiedImage != null && !string.IsNullOrEmpty(arguments.Text))
            {
                ImageProccessing.AddTextToImage(arguments, modifiedImage);
                //if (!mainPictureBox.Image.Equals(image))
                //{
                //    mainPictureBox.Image.Dispose();
                //}
                mainPictureBox.Image = modifiedImage;
            }
        }

        private void ApplyText(TextArguments arguments)
        {
            if (_bitmaps.Count == 0)
            {
                return;
            }

            var image = _bitmaps.Peek();
            var modifiedImage = new Bitmap(image);

            if (modifiedImage == null || string.IsNullOrEmpty(arguments.Text))
            {
                return;
            }

            ImageProccessing.AddTextToImage(arguments, modifiedImage);
            _bitmaps.Push(modifiedImage);
            mainPictureBox.Image = modifiedImage;
        }

        private void OnCropToolStripMenuItemClick(object sender, EventArgs e)
        {
            if (_bitmaps.Count == 0)
            {
                MessageBox.Show("No Image Selected");
                return;
            }

            if (_currentOperation == Operation.Cropping)
            {
                return;
            }

            var image = _bitmaps.Peek();
            _currentOperation = Operation.Cropping;
            sidePanel.Controls.Clear();
            var controller = new CropController(OnApplyCrop) { Dock = DockStyle.Fill };
            sidePanel.Controls.Add(controller);
        }

        private void OnApplyCrop()
        {
            var originalImage = _bitmaps.Peek();
            var croppedImage = ImageProccessing.Crop(originalImage, _selectionRectangle);
            _selectionRectangle = Rectangle.Empty;
            _bitmaps.Push(croppedImage);
            mainPictureBox.Image = _bitmaps.Peek();
            mainPictureBox.Invalidate();
        }

        private void OnAddAudioToolStripMenuItemClick(object sender, EventArgs e)
        {
            if (_bitmaps.Count == 0)
            {
                MessageBox.Show("No Image Selected");
                return;
            }

            if (_currentOperation == Operation.AddingAudio)
            {
                return;
            }

            _currentOperation = Operation.AddingAudio;
            sidePanel.Controls.Clear();
            var controller = new AddAudioController() { Dock = DockStyle.Fill };
            sidePanel.Controls.Add(controller);
        }

        private void SharpenToolStripMenuItemClick(object sender, EventArgs e)
        {
            if (_bitmaps.Count == 0)
            {
                MessageBox.Show("No Image Selected");
                return;
            }

            if (_currentOperation == Operation.Sharpening)
            {
                return;
            }

            _currentOperation = Operation.Sharpening;
            sidePanel.Controls.Clear();
            var controller = new SharpenController(OnSharpenTrackBarChange, OnApplySharpenClick)
            {
                Dock = DockStyle.Fill
            };
            sidePanel.Controls.Add(controller);
        }

        private void OnApplySharpenClick(int value)
        {
            var originalImage = _bitmaps.Peek();
            var sharpedImage = ImageProccessing.Sharpen(originalImage, value);
            _bitmaps.Push(sharpedImage);
            mainPictureBox.Image = _bitmaps.Peek();
        }

        private void OnSharpenTrackBarChange(bool preview, int value)
        {
            if (!preview)
            {
                mainPictureBox.Image = _bitmaps.Peek();
                return;
            }

            var originalImage = _bitmaps.Peek();
            var sharpedImage = ImageProccessing.Sharpen(originalImage, value);
            mainPictureBox.Image = sharpedImage;
        }

        private void OnSearchToolStripMenuItemClick(object sender, EventArgs e)
        {
            if (_seachFormOpend)
            {
                return;
            }

            var form = new SearchForm(LoadSelectedImage);
            form.FormClosed += (sender, e) => _seachFormOpend = false;
            form.Show();
        }

        private void LoadSelectedImage(string path)
        {
            if (_bitmaps.Count > 1 && !DiscardChanges())
            {
                return;
            }

            _bitmaps.Clear();
            var image = new Bitmap(path);
            _bitmaps.Push(image);
            mainPictureBox.Image = image;
        }

        private void OnExportPDFToolStripMenuItemClick(object sender, EventArgs e)
        {
            if (_bitmaps.Count == 0)
            {
                MessageBox.Show("No Image Selected");
                return;
            }

            var form = new PDFForm(
                _progress,
                classificationToolStripDropDownButton.Text!,
                new Bitmap(mainPictureBox.Image)
            );

            form.ShowDialog();
        }

        private void OnExportZipToolStripMenuItemClick(object sender, EventArgs e)
        {
            if (_bitmaps.Count == 0)
            {
                MessageBox.Show("No Image Selected");
                return;
            }

            var form = new ExportZipForm(
                new Bitmap(mainPictureBox.Image),
                _progress,
                classificationToolStripDropDownButton.Text!
            );

            form.ShowDialog();
        }

        private static bool DiscardChanges()
        {
            var result = MessageBox.Show(
                "All changes will be lost",
                "Discard",
                MessageBoxButtons.OKCancel,
                MessageBoxIcon.Warning
            );

            return result == DialogResult.OK;
        }

        private void OnFormClosing(object sender, FormClosingEventArgs e)
        {
            if (_bitmaps.Count > 1 && !DiscardChanges())
            {
                MessageBox.Show("No Image Selected");
                e.Cancel = true;
            }
        }

        private async void OnShareOnWhatsappToolStripMenuItemClick(object sender, EventArgs e)
        {
            if (_bitmaps.Count == 0)
            {
                MessageBox.Show("No Image Selected");
                return;
            }

            Enabled = false;

            try
            {
                var shareResult = await ShareHelper.ShareImage(new Bitmap(mainPictureBox.Image));
                if (!string.IsNullOrEmpty(shareResult?.Link))
                {
                    ShareHelper.OpenFileUsingDefaultProgram(
                        ShareHelper.WhatsappUrl(
                            $"Here is your examination image {shareResult.Link} you can access it before the {shareResult.Expires:yyyy-MM-dd HH:mm}"
                        )
                    );
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Something went wrong, check your Internet connection");
            }

            Enabled = true;
        }
    }
}
