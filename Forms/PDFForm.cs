﻿using System.IO.Compression;
using Multimedia.Helpers;
using QuestPDF.Fluent;

namespace Multimedia.Forms
{
    public partial class PDFForm : Form
    {
        private readonly string _progress;
        private readonly string _classification;
        private readonly Bitmap _image;

        public PDFForm(string progress, string classification, Bitmap image)
        {
            InitializeComponent();
            _progress = progress;
            _classification = classification;
            _image = image;
            progressLabel.Text = _progress;
            classifcationLabel.Text = _classification;
        }

        private async void OnExportButtonClick(object sender, EventArgs e)
        {
            exportButton.Enabled = false;
            sharePDFButton.Enabled = false;

            var document = PDFGenerator.GeneratePDFReport(
                _image,
                nameTextBox.Text,
                _progress,
                _classification,
                detailsRichTextBox.Text
            );

            bool share = ((Button)sender).Name == "sharePDFButton";
            if (!share)
            {
                document.GeneratePdfAndShow();
                exportButton.Enabled = true;
                sharePDFButton.Enabled = true;
                return;
            }

            try
            {
                var tempPath = Path.GetTempFileName() + ".pdf";
                document.GeneratePdf(tempPath);
                var shareResult = await ShareHelper.SharePDFFile(tempPath);
                if (!string.IsNullOrEmpty(shareResult?.Link))
                {
                    ShareHelper.OpenFileUsingDefaultProgram(
                        ShareHelper.WhatsappUrl(
                            $"Here is your report PDF {shareResult.Link} you can access it before the {shareResult.Expires:yyyy-MM-dd HH:mm}"
                        )
                    );
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Something went wrong, check your Internet connection");
            }

            exportButton.Enabled = true;
            sharePDFButton.Enabled = true;
        }
    }
}
