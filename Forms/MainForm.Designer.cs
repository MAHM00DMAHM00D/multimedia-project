﻿namespace Multimedia.Forms
{
    partial class MainForm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            menuStrip = new MenuStrip();
            fileToolStripMenuItem = new ToolStripMenuItem();
            openToolStripMenuItem = new ToolStripMenuItem();
            saveAsToolStripMenuItem = new ToolStripMenuItem();
            exportPDFToolStripMenuItem = new ToolStripMenuItem();
            exportZipToolStripMenuItem = new ToolStripMenuItem();
            editToolStripMenuItem = new ToolStripMenuItem();
            undoToolStripMenuItem = new ToolStripMenuItem();
            compareToolStripMenuItem = new ToolStripMenuItem();
            selectToBendToolStripMenuItem = new ToolStripMenuItem();
            addTextToolStripMenuItem = new ToolStripMenuItem();
            addAudioToolStripMenuItem = new ToolStripMenuItem();
            cropToolStripMenuItem = new ToolStripMenuItem();
            sharpenToolStripMenuItem = new ToolStripMenuItem();
            searchToolStripMenuItem = new ToolStripMenuItem();
            mainPictureBox = new PictureBox();
            picturePanel = new Panel();
            statusStrip = new StatusStrip();
            progressToolStripStatusLabel = new ToolStripStatusLabel();
            progressValueToolStripStatusLabel = new ToolStripStatusLabel();
            classificationToolStripStatusLabel = new ToolStripStatusLabel();
            classificationToolStripDropDownButton = new ToolStripDropDownButton();
            highToolStripMenuItem = new ToolStripMenuItem();
            midiumToolStripMenuItem = new ToolStripMenuItem();
            lowToolStripMenuItem = new ToolStripMenuItem();
            noneToolStripMenuItem = new ToolStripMenuItem();
            sidePanel = new Panel();
            shareOnToolStripMenuItem = new ToolStripMenuItem();
            whatsappToolStripMenuItem = new ToolStripMenuItem();
            menuStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)mainPictureBox).BeginInit();
            picturePanel.SuspendLayout();
            statusStrip.SuspendLayout();
            SuspendLayout();
            // 
            // menuStrip
            // 
            menuStrip.Items.AddRange(new ToolStripItem[] { fileToolStripMenuItem, editToolStripMenuItem, searchToolStripMenuItem });
            menuStrip.Location = new Point(0, 0);
            menuStrip.Name = "menuStrip";
            menuStrip.Size = new Size(823, 24);
            menuStrip.TabIndex = 0;
            menuStrip.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            fileToolStripMenuItem.DropDownItems.AddRange(new ToolStripItem[] { openToolStripMenuItem, saveAsToolStripMenuItem, exportPDFToolStripMenuItem, exportZipToolStripMenuItem, shareOnToolStripMenuItem });
            fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            fileToolStripMenuItem.Size = new Size(37, 20);
            fileToolStripMenuItem.Text = "&File";
            // 
            // openToolStripMenuItem
            // 
            openToolStripMenuItem.Name = "openToolStripMenuItem";
            openToolStripMenuItem.ShortcutKeys = Keys.Control | Keys.O;
            openToolStripMenuItem.Size = new Size(186, 22);
            openToolStripMenuItem.Text = "&Open";
            openToolStripMenuItem.Click += OpenToolStripMenuItemClick;
            // 
            // saveAsToolStripMenuItem
            // 
            saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
            saveAsToolStripMenuItem.ShortcutKeys = Keys.Control | Keys.Shift | Keys.S;
            saveAsToolStripMenuItem.Size = new Size(186, 22);
            saveAsToolStripMenuItem.Text = "&Save As";
            saveAsToolStripMenuItem.Click += SaveAsToolStripMenuItemClick;
            // 
            // exportPDFToolStripMenuItem
            // 
            exportPDFToolStripMenuItem.Name = "exportPDFToolStripMenuItem";
            exportPDFToolStripMenuItem.Size = new Size(186, 22);
            exportPDFToolStripMenuItem.Text = "&Export PDF";
            exportPDFToolStripMenuItem.Click += OnExportPDFToolStripMenuItemClick;
            // 
            // exportZipToolStripMenuItem
            // 
            exportZipToolStripMenuItem.Name = "exportZipToolStripMenuItem";
            exportZipToolStripMenuItem.Size = new Size(186, 22);
            exportZipToolStripMenuItem.Text = "Export &Zip";
            exportZipToolStripMenuItem.Click += OnExportZipToolStripMenuItemClick;
            // 
            // editToolStripMenuItem
            // 
            editToolStripMenuItem.DropDownItems.AddRange(new ToolStripItem[] { undoToolStripMenuItem, compareToolStripMenuItem, selectToBendToolStripMenuItem, addTextToolStripMenuItem, addAudioToolStripMenuItem, cropToolStripMenuItem, sharpenToolStripMenuItem });
            editToolStripMenuItem.Name = "editToolStripMenuItem";
            editToolStripMenuItem.Size = new Size(39, 20);
            editToolStripMenuItem.Text = "&Edit";
            // 
            // undoToolStripMenuItem
            // 
            undoToolStripMenuItem.Name = "undoToolStripMenuItem";
            undoToolStripMenuItem.ShortcutKeys = Keys.Control | Keys.Z;
            undoToolStripMenuItem.Size = new Size(153, 22);
            undoToolStripMenuItem.Text = "&Undo";
            undoToolStripMenuItem.Click += UndoToolStripMenuItemClick;
            // 
            // compareToolStripMenuItem
            // 
            compareToolStripMenuItem.Name = "compareToolStripMenuItem";
            compareToolStripMenuItem.Size = new Size(153, 22);
            compareToolStripMenuItem.Text = "&Compare";
            compareToolStripMenuItem.Click += CompareToolStripMenuItemClick;
            // 
            // selectToBendToolStripMenuItem
            // 
            selectToBendToolStripMenuItem.Name = "selectToBendToolStripMenuItem";
            selectToBendToolStripMenuItem.Size = new Size(153, 22);
            selectToBendToolStripMenuItem.Text = "&Select To Blend";
            selectToBendToolStripMenuItem.Click += OnSelectToBendToolStripMenuItemClick;
            // 
            // addTextToolStripMenuItem
            // 
            addTextToolStripMenuItem.Name = "addTextToolStripMenuItem";
            addTextToolStripMenuItem.Size = new Size(153, 22);
            addTextToolStripMenuItem.Text = "Add &Text";
            addTextToolStripMenuItem.Click += OnAddTextToolStripMenuItemClick;
            // 
            // addAudioToolStripMenuItem
            // 
            addAudioToolStripMenuItem.Name = "addAudioToolStripMenuItem";
            addAudioToolStripMenuItem.Size = new Size(153, 22);
            addAudioToolStripMenuItem.Text = "Add &Audio";
            addAudioToolStripMenuItem.Click += OnAddAudioToolStripMenuItemClick;
            // 
            // cropToolStripMenuItem
            // 
            cropToolStripMenuItem.Name = "cropToolStripMenuItem";
            cropToolStripMenuItem.Size = new Size(153, 22);
            cropToolStripMenuItem.Text = "C&rop";
            cropToolStripMenuItem.Click += OnCropToolStripMenuItemClick;
            // 
            // sharpenToolStripMenuItem
            // 
            sharpenToolStripMenuItem.Name = "sharpenToolStripMenuItem";
            sharpenToolStripMenuItem.Size = new Size(153, 22);
            sharpenToolStripMenuItem.Text = "S&harpen";
            sharpenToolStripMenuItem.Click += SharpenToolStripMenuItemClick;
            // 
            // searchToolStripMenuItem
            // 
            searchToolStripMenuItem.Name = "searchToolStripMenuItem";
            searchToolStripMenuItem.Size = new Size(54, 20);
            searchToolStripMenuItem.Text = "Search";
            searchToolStripMenuItem.Click += OnSearchToolStripMenuItemClick;
            // 
            // mainPictureBox
            // 
            mainPictureBox.Dock = DockStyle.Fill;
            mainPictureBox.Location = new Point(0, 0);
            mainPictureBox.Name = "mainPictureBox";
            mainPictureBox.Size = new Size(537, 494);
            mainPictureBox.SizeMode = PictureBoxSizeMode.AutoSize;
            mainPictureBox.TabIndex = 1;
            mainPictureBox.TabStop = false;
            mainPictureBox.Paint += MainPictureBoxPaint;
            mainPictureBox.MouseDown += MainPictureBoxMouseDown;
            mainPictureBox.MouseMove += MainPictureBoxMouseMove;
            mainPictureBox.MouseUp += MainPictureBoxMouseUp;
            // 
            // picturePanel
            // 
            picturePanel.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
            picturePanel.AutoScroll = true;
            picturePanel.BorderStyle = BorderStyle.FixedSingle;
            picturePanel.Controls.Add(mainPictureBox);
            picturePanel.Location = new Point(12, 27);
            picturePanel.Name = "picturePanel";
            picturePanel.Size = new Size(539, 496);
            picturePanel.TabIndex = 2;
            // 
            // statusStrip
            // 
            statusStrip.Items.AddRange(new ToolStripItem[] { progressToolStripStatusLabel, progressValueToolStripStatusLabel, classificationToolStripStatusLabel, classificationToolStripDropDownButton });
            statusStrip.Location = new Point(0, 526);
            statusStrip.Name = "statusStrip";
            statusStrip.Size = new Size(823, 22);
            statusStrip.TabIndex = 7;
            statusStrip.Text = "statusStrip1";
            // 
            // progressToolStripStatusLabel
            // 
            progressToolStripStatusLabel.Name = "progressToolStripStatusLabel";
            progressToolStripStatusLabel.Size = new Size(55, 17);
            progressToolStripStatusLabel.Text = "Progress:";
            // 
            // progressValueToolStripStatusLabel
            // 
            progressValueToolStripStatusLabel.AutoSize = false;
            progressValueToolStripStatusLabel.Name = "progressValueToolStripStatusLabel";
            progressValueToolStripStatusLabel.Size = new Size(120, 17);
            progressValueToolStripStatusLabel.Text = "None";
            progressValueToolStripStatusLabel.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // classificationToolStripStatusLabel
            // 
            classificationToolStripStatusLabel.Name = "classificationToolStripStatusLabel";
            classificationToolStripStatusLabel.Size = new Size(80, 17);
            classificationToolStripStatusLabel.Text = "Classification:";
            // 
            // classificationToolStripDropDownButton
            // 
            classificationToolStripDropDownButton.AutoSize = false;
            classificationToolStripDropDownButton.DisplayStyle = ToolStripItemDisplayStyle.Text;
            classificationToolStripDropDownButton.DropDownItems.AddRange(new ToolStripItem[] { highToolStripMenuItem, midiumToolStripMenuItem, lowToolStripMenuItem, noneToolStripMenuItem });
            classificationToolStripDropDownButton.Image = (Image)resources.GetObject("classificationToolStripDropDownButton.Image");
            classificationToolStripDropDownButton.ImageTransparentColor = Color.Magenta;
            classificationToolStripDropDownButton.Name = "classificationToolStripDropDownButton";
            classificationToolStripDropDownButton.Size = new Size(120, 20);
            classificationToolStripDropDownButton.Text = "None";
            // 
            // highToolStripMenuItem
            // 
            highToolStripMenuItem.Name = "highToolStripMenuItem";
            highToolStripMenuItem.Size = new Size(119, 22);
            highToolStripMenuItem.Text = "High";
            highToolStripMenuItem.Click += OnClassificationToolStripMenuItemClick;
            // 
            // midiumToolStripMenuItem
            // 
            midiumToolStripMenuItem.Name = "midiumToolStripMenuItem";
            midiumToolStripMenuItem.Size = new Size(119, 22);
            midiumToolStripMenuItem.Text = "Medium";
            midiumToolStripMenuItem.Click += OnClassificationToolStripMenuItemClick;
            // 
            // lowToolStripMenuItem
            // 
            lowToolStripMenuItem.Name = "lowToolStripMenuItem";
            lowToolStripMenuItem.Size = new Size(119, 22);
            lowToolStripMenuItem.Text = "Low";
            lowToolStripMenuItem.Click += OnClassificationToolStripMenuItemClick;
            // 
            // noneToolStripMenuItem
            // 
            noneToolStripMenuItem.Name = "noneToolStripMenuItem";
            noneToolStripMenuItem.Size = new Size(119, 22);
            noneToolStripMenuItem.Text = "None";
            noneToolStripMenuItem.Click += OnClassificationToolStripMenuItemClick;
            // 
            // sidePanel
            // 
            sidePanel.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Right;
            sidePanel.Location = new Point(556, 28);
            sidePanel.Name = "sidePanel";
            sidePanel.Size = new Size(260, 494);
            sidePanel.TabIndex = 8;
            // 
            // shareOnToolStripMenuItem
            // 
            shareOnToolStripMenuItem.DropDownItems.AddRange(new ToolStripItem[] { whatsappToolStripMenuItem });
            shareOnToolStripMenuItem.Name = "shareOnToolStripMenuItem";
            shareOnToolStripMenuItem.Size = new Size(186, 22);
            shareOnToolStripMenuItem.Text = "Share Image On";
            // 
            // whatsappToolStripMenuItem
            // 
            whatsappToolStripMenuItem.Name = "whatsappToolStripMenuItem";
            whatsappToolStripMenuItem.Size = new Size(180, 22);
            whatsappToolStripMenuItem.Text = "Whatsapp";
            whatsappToolStripMenuItem.Click += OnShareOnWhatsappToolStripMenuItemClick;
            // 
            // MainForm
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(823, 548);
            Controls.Add(sidePanel);
            Controls.Add(statusStrip);
            Controls.Add(picturePanel);
            Controls.Add(menuStrip);
            MainMenuStrip = menuStrip;
            MinimumSize = new Size(512, 512);
            Name = "MainForm";
            Text = "Multimedia";
            FormClosing += OnFormClosing;
            menuStrip.ResumeLayout(false);
            menuStrip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)mainPictureBox).EndInit();
            picturePanel.ResumeLayout(false);
            picturePanel.PerformLayout();
            statusStrip.ResumeLayout(false);
            statusStrip.PerformLayout();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private MenuStrip menuStrip;
        private ToolStripMenuItem fileToolStripMenuItem;
        private ToolStripMenuItem openToolStripMenuItem;
        private ToolStripMenuItem saveAsToolStripMenuItem;
        private PictureBox mainPictureBox;
        private Panel picturePanel;
        private ToolStripMenuItem editToolStripMenuItem;
        private ToolStripMenuItem undoToolStripMenuItem;
        private ListBox listBox1;
        private ToolStripMenuItem compareToolStripMenuItem;
        private StatusStrip statusStrip;
        private ToolStripStatusLabel progressToolStripStatusLabel;
        private ToolStripStatusLabel progressValueToolStripStatusLabel;
        private ToolStripStatusLabel classificationToolStripStatusLabel;
        private ToolStripDropDownButton classificationToolStripDropDownButton;
        private ToolStripMenuItem highToolStripMenuItem;
        private ToolStripMenuItem midiumToolStripMenuItem;
        private ToolStripMenuItem lowToolStripMenuItem;
        private ToolStripMenuItem noneToolStripMenuItem;
        private ToolStripMenuItem addTextToolStripMenuItem;
        private Panel sidePanel;
        private ToolStripMenuItem selectToBendToolStripMenuItem;
        private ToolStripMenuItem cropToolStripMenuItem;
        private ToolStripMenuItem addAudioToolStripMenuItem;
        private ToolStripMenuItem sharpenToolStripMenuItem;
        private ToolStripMenuItem searchToolStripMenuItem;
        private ToolStripMenuItem exportPDFToolStripMenuItem;
        private ToolStripMenuItem exportZipToolStripMenuItem;
        private ToolStripMenuItem shareOnToolStripMenuItem;
        private ToolStripMenuItem whatsappToolStripMenuItem;
    }
}
