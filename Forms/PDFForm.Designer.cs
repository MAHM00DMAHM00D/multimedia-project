﻿namespace Multimedia.Forms
{
    partial class PDFForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            exportButton = new Button();
            label1 = new Label();
            label2 = new Label();
            label3 = new Label();
            nameTextBox = new TextBox();
            label4 = new Label();
            label5 = new Label();
            detailsRichTextBox = new RichTextBox();
            progressLabel = new Label();
            classifcationLabel = new Label();
            sharePDFButton = new Button();
            SuspendLayout();
            // 
            // exportButton
            // 
            exportButton.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            exportButton.Location = new Point(330, 417);
            exportButton.Name = "exportButton";
            exportButton.Size = new Size(85, 23);
            exportButton.TabIndex = 1;
            exportButton.Text = "Export PDF";
            exportButton.UseVisualStyleBackColor = true;
            exportButton.Click += OnExportButtonClick;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Font = new Font("Segoe UI", 9F, FontStyle.Bold);
            label1.Location = new Point(12, 14);
            label1.Margin = new Padding(3, 3, 3, 12);
            label1.Name = "label1";
            label1.Size = new Size(95, 15);
            label1.TabIndex = 2;
            label1.Text = "Document Info:";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Font = new Font("Segoe UI", 9F, FontStyle.Bold);
            label2.Location = new Point(12, 44);
            label2.Margin = new Padding(3, 3, 3, 12);
            label2.Name = "label2";
            label2.Size = new Size(58, 15);
            label2.TabIndex = 2;
            label2.Text = "Progress:";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Font = new Font("Segoe UI", 9F, FontStyle.Bold);
            label3.Location = new Point(12, 74);
            label3.Margin = new Padding(3, 3, 3, 12);
            label3.Name = "label3";
            label3.Size = new Size(81, 15);
            label3.TabIndex = 2;
            label3.Text = "Classification:";
            // 
            // nameTextBox
            // 
            nameTextBox.Location = new Point(100, 101);
            nameTextBox.Name = "nameTextBox";
            nameTextBox.Size = new Size(315, 23);
            nameTextBox.TabIndex = 3;
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Font = new Font("Segoe UI", 9F, FontStyle.Bold);
            label4.Location = new Point(12, 104);
            label4.Margin = new Padding(3, 3, 3, 12);
            label4.Name = "label4";
            label4.Size = new Size(86, 15);
            label4.TabIndex = 2;
            label4.Text = "Patient Name:";
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Font = new Font("Segoe UI", 9F, FontStyle.Bold);
            label5.Location = new Point(12, 134);
            label5.Margin = new Padding(3, 3, 3, 12);
            label5.Name = "label5";
            label5.Size = new Size(81, 15);
            label5.TabIndex = 2;
            label5.Text = "More Details:";
            // 
            // detailsRichTextBox
            // 
            detailsRichTextBox.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
            detailsRichTextBox.Location = new Point(12, 164);
            detailsRichTextBox.Name = "detailsRichTextBox";
            detailsRichTextBox.Size = new Size(403, 247);
            detailsRichTextBox.TabIndex = 4;
            detailsRichTextBox.Text = "";
            // 
            // progressLabel
            // 
            progressLabel.AutoSize = true;
            progressLabel.Location = new Point(100, 44);
            progressLabel.Margin = new Padding(3, 3, 3, 12);
            progressLabel.Name = "progressLabel";
            progressLabel.Size = new Size(0, 15);
            progressLabel.TabIndex = 2;
            // 
            // classifcationLabel
            // 
            classifcationLabel.AutoSize = true;
            classifcationLabel.Location = new Point(100, 74);
            classifcationLabel.Margin = new Padding(3, 3, 3, 12);
            classifcationLabel.Name = "classifcationLabel";
            classifcationLabel.Size = new Size(0, 15);
            classifcationLabel.TabIndex = 2;
            // 
            // sharePDFButton
            // 
            sharePDFButton.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            sharePDFButton.Location = new Point(239, 417);
            sharePDFButton.Name = "sharePDFButton";
            sharePDFButton.Size = new Size(85, 23);
            sharePDFButton.TabIndex = 1;
            sharePDFButton.Text = "Share PDF";
            sharePDFButton.UseVisualStyleBackColor = true;
            sharePDFButton.Click += OnExportButtonClick;
            // 
            // PDFForm
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(428, 450);
            Controls.Add(detailsRichTextBox);
            Controls.Add(nameTextBox);
            Controls.Add(label5);
            Controls.Add(label4);
            Controls.Add(label3);
            Controls.Add(classifcationLabel);
            Controls.Add(progressLabel);
            Controls.Add(label2);
            Controls.Add(label1);
            Controls.Add(sharePDFButton);
            Controls.Add(exportButton);
            MinimumSize = new Size(444, 489);
            Name = "PDFForm";
            Text = "Export PDF";
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion
        private Button exportButton;
        private Label label1;
        private Label label2;
        private Label label3;
        private TextBox nameTextBox;
        private Label label4;
        private Label label5;
        private RichTextBox detailsRichTextBox;
        private Label progressLabel;
        private Label classifcationLabel;
        private Button sharePDFButton;
    }
}