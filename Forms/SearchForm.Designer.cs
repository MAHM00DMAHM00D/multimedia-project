﻿namespace Multimedia.Forms
{
    partial class SearchForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            imageList = new ImageList(components);
            listView = new ListView();
            columnHeader1 = new ColumnHeader();
            columnHeader2 = new ColumnHeader();
            columnHeader3 = new ColumnHeader();
            columnHeader5 = new ColumnHeader();
            columnHeader4 = new ColumnHeader();
            dateTimePicker = new DateTimePicker();
            button1 = new Button();
            widthUpDown = new NumericUpDown();
            heightUpDown = new NumericUpDown();
            dateRadioButton = new RadioButton();
            widthHeightRadioButton = new RadioButton();
            label3 = new Label();
            label4 = new Label();
            selectedFolderTextBox = new TextBox();
            selectFolderButton = new Button();
            progressBar = new ProgressBar();
            listViewComboBox = new ComboBox();
            label1 = new Label();
            loadSelectedButton = new Button();
            ((System.ComponentModel.ISupportInitialize)widthUpDown).BeginInit();
            ((System.ComponentModel.ISupportInitialize)heightUpDown).BeginInit();
            SuspendLayout();
            // 
            // imageList
            // 
            imageList.ColorDepth = ColorDepth.Depth32Bit;
            imageList.ImageSize = new Size(32, 32);
            imageList.TransparentColor = Color.Transparent;
            // 
            // listView
            // 
            listView.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
            listView.Columns.AddRange(new ColumnHeader[] { columnHeader1, columnHeader2, columnHeader3, columnHeader5, columnHeader4 });
            listView.LargeImageList = imageList;
            listView.Location = new Point(12, 98);
            listView.Name = "listView";
            listView.Size = new Size(776, 418);
            listView.SmallImageList = imageList;
            listView.TabIndex = 0;
            listView.UseCompatibleStateImageBehavior = false;
            listView.View = View.Details;
            // 
            // columnHeader1
            // 
            columnHeader1.Text = "File";
            columnHeader1.Width = 120;
            // 
            // columnHeader2
            // 
            columnHeader2.Text = "Width";
            columnHeader2.Width = 90;
            // 
            // columnHeader3
            // 
            columnHeader3.Text = "Height";
            columnHeader3.Width = 90;
            // 
            // columnHeader5
            // 
            columnHeader5.Text = "Creation Date";
            columnHeader5.Width = 120;
            // 
            // columnHeader4
            // 
            columnHeader4.Text = "Modified Date";
            columnHeader4.Width = 120;
            // 
            // dateTimePicker
            // 
            dateTimePicker.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            dateTimePicker.Enabled = false;
            dateTimePicker.Format = DateTimePickerFormat.Short;
            dateTimePicker.Location = new Point(32, 41);
            dateTimePicker.Name = "dateTimePicker";
            dateTimePicker.Size = new Size(345, 23);
            dateTimePicker.TabIndex = 1;
            // 
            // button1
            // 
            button1.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            button1.Enabled = false;
            button1.Location = new Point(713, 41);
            button1.Name = "button1";
            button1.Size = new Size(75, 23);
            button1.TabIndex = 2;
            button1.Text = "Search";
            button1.UseVisualStyleBackColor = true;
            button1.Click += OnSearchButtonClick;
            // 
            // widthUpDown
            // 
            widthUpDown.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            widthUpDown.Enabled = false;
            widthUpDown.Location = new Point(448, 41);
            widthUpDown.Maximum = new decimal(new int[] { 10000, 0, 0, 0 });
            widthUpDown.Name = "widthUpDown";
            widthUpDown.Size = new Size(102, 23);
            widthUpDown.TabIndex = 3;
            widthUpDown.Value = new decimal(new int[] { 512, 0, 0, 0 });
            // 
            // heightUpDown
            // 
            heightUpDown.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            heightUpDown.Enabled = false;
            heightUpDown.Location = new Point(605, 41);
            heightUpDown.Maximum = new decimal(new int[] { 10000, 0, 0, 0 });
            heightUpDown.Name = "heightUpDown";
            heightUpDown.Size = new Size(102, 23);
            heightUpDown.TabIndex = 3;
            heightUpDown.Value = new decimal(new int[] { 512, 0, 0, 0 });
            // 
            // dateRadioButton
            // 
            dateRadioButton.AutoSize = true;
            dateRadioButton.Checked = true;
            dateRadioButton.Enabled = false;
            dateRadioButton.Location = new Point(12, 41);
            dateRadioButton.Name = "dateRadioButton";
            dateRadioButton.Size = new Size(14, 13);
            dateRadioButton.TabIndex = 4;
            dateRadioButton.TabStop = true;
            dateRadioButton.UseVisualStyleBackColor = true;
            // 
            // widthHeightRadioButton
            // 
            widthHeightRadioButton.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            widthHeightRadioButton.Enabled = false;
            widthHeightRadioButton.Location = new Point(383, 41);
            widthHeightRadioButton.Name = "widthHeightRadioButton";
            widthHeightRadioButton.Size = new Size(14, 20);
            widthHeightRadioButton.TabIndex = 4;
            widthHeightRadioButton.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            label3.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            label3.AutoSize = true;
            label3.Location = new Point(556, 41);
            label3.Name = "label3";
            label3.Size = new Size(43, 15);
            label3.TabIndex = 5;
            label3.Text = "Height";
            // 
            // label4
            // 
            label4.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            label4.AutoSize = true;
            label4.Location = new Point(403, 41);
            label4.Name = "label4";
            label4.Size = new Size(39, 15);
            label4.TabIndex = 5;
            label4.Text = "Width";
            // 
            // selectedFolderTextBox
            // 
            selectedFolderTextBox.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            selectedFolderTextBox.Location = new Point(12, 12);
            selectedFolderTextBox.Name = "selectedFolderTextBox";
            selectedFolderTextBox.ReadOnly = true;
            selectedFolderTextBox.Size = new Size(639, 23);
            selectedFolderTextBox.TabIndex = 6;
            // 
            // selectFolderButton
            // 
            selectFolderButton.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            selectFolderButton.Location = new Point(657, 12);
            selectFolderButton.Name = "selectFolderButton";
            selectFolderButton.Size = new Size(131, 23);
            selectFolderButton.TabIndex = 2;
            selectFolderButton.Text = "Select Folder";
            selectFolderButton.UseVisualStyleBackColor = true;
            selectFolderButton.Click += OnSelectFolderButtonClick;
            // 
            // progressBar
            // 
            progressBar.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
            progressBar.Location = new Point(12, 522);
            progressBar.Name = "progressBar";
            progressBar.Size = new Size(776, 23);
            progressBar.TabIndex = 7;
            // 
            // listViewComboBox
            // 
            listViewComboBox.FormattingEnabled = true;
            listViewComboBox.Items.AddRange(new object[] { "List", "Details", "LargeIcon" });
            listViewComboBox.Location = new Point(667, 70);
            listViewComboBox.Name = "listViewComboBox";
            listViewComboBox.Size = new Size(121, 23);
            listViewComboBox.TabIndex = 8;
            listViewComboBox.Text = "Details";
            listViewComboBox.SelectedIndexChanged += OnListViewComboBoxSelectedIndexChanged;
            // 
            // label1
            // 
            label1.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            label1.AutoSize = true;
            label1.Location = new Point(618, 70);
            label1.Name = "label1";
            label1.Size = new Size(46, 15);
            label1.TabIndex = 5;
            label1.Text = "View as";
            // 
            // loadSelectedButton
            // 
            loadSelectedButton.Enabled = false;
            loadSelectedButton.Location = new Point(12, 70);
            loadSelectedButton.Name = "loadSelectedButton";
            loadSelectedButton.Size = new Size(120, 23);
            loadSelectedButton.TabIndex = 2;
            loadSelectedButton.Text = "Load Selected";
            loadSelectedButton.UseVisualStyleBackColor = true;
            loadSelectedButton.Click += OnLoadSelectedButtonClick;
            // 
            // SearchForm
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(800, 557);
            Controls.Add(listViewComboBox);
            Controls.Add(progressBar);
            Controls.Add(selectedFolderTextBox);
            Controls.Add(label4);
            Controls.Add(label1);
            Controls.Add(label3);
            Controls.Add(widthHeightRadioButton);
            Controls.Add(dateRadioButton);
            Controls.Add(heightUpDown);
            Controls.Add(widthUpDown);
            Controls.Add(selectFolderButton);
            Controls.Add(loadSelectedButton);
            Controls.Add(button1);
            Controls.Add(dateTimePicker);
            Controls.Add(listView);
            MinimumSize = new Size(816, 596);
            Name = "SearchForm";
            Text = "SearchForm";
            ((System.ComponentModel.ISupportInitialize)widthUpDown).EndInit();
            ((System.ComponentModel.ISupportInitialize)heightUpDown).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private ImageList imageList;
        private ListView listView;
        private ColumnHeader columnHeader1;
        private ColumnHeader columnHeader2;
        private ColumnHeader columnHeader3;
        private ColumnHeader columnHeader4;
        private DateTimePicker dateTimePicker;
        private Button button1;
        private NumericUpDown widthUpDown;
        private NumericUpDown heightUpDown;
        private RadioButton dateRadioButton;
        private RadioButton widthHeightRadioButton;
        private Label label3;
        private Label label4;
        private TextBox selectedFolderTextBox;
        private Button selectFolderButton;
        private ProgressBar progressBar;
        private ColumnHeader columnHeader5;
        private ComboBox listViewComboBox;
        private Label label1;
        private Button loadSelectedButton;
    }
}