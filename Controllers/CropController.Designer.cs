﻿namespace Multimedia.Controllers
{
    partial class CropController
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            applyCropButton = new Button();
            SuspendLayout();
            // 
            // applyCropButton
            // 
            applyCropButton.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            applyCropButton.Location = new Point(3, 172);
            applyCropButton.Name = "applyCropButton";
            applyCropButton.Size = new Size(251, 23);
            applyCropButton.TabIndex = 10;
            applyCropButton.Text = "Apply Crop";
            applyCropButton.UseVisualStyleBackColor = true;
            applyCropButton.Click += OnApplyBlendingButtonClick;
            // 
            // CropController
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            Controls.Add(applyCropButton);
            Name = "CropController";
            Size = new Size(257, 198);
            ResumeLayout(false);
        }

        #endregion
        private Button applyCropButton;
    }
}
