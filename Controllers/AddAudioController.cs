﻿using NAudio.Wave;

namespace Multimedia.Controllers
{
    public partial class AddAudioController : UserControl
    {
        private WaveOutEvent? _outputDevice;
        private AudioFileReader? _audioFile;
        bool _playRecorded;
        string? _outputFilePath;
        string? _selectedFilePath;
        WaveInEvent? _waveInEvent;
        WaveFileWriter? _waveWriter;
        bool _recording = false;

        public AddAudioController()
        {
            InitializeComponent();
            _waveInEvent = new() { WaveFormat = new WaveFormat() };
            _waveInEvent.DataAvailable += OnDataAvailable;
            _waveInEvent.RecordingStopped += OnRecordingStopped;
        }

        public void OnDataAvailable(object? sender, WaveInEventArgs a)
        {
            _waveWriter!.Write(a.Buffer, 0, a.BytesRecorded);
            if (_waveWriter?.Position > _waveInEvent?.WaveFormat.AverageBytesPerSecond * 30)
            {
                _waveInEvent?.StopRecording();
            }
        }

        public void OnRecordingStopped(object? sender, StoppedEventArgs e)
        {
            _waveWriter?.Dispose();
            _waveWriter = null;
        }

        private void OnRecordButtonClick(object sender, EventArgs e)
        {
            if (!_recording)
            {
                SaveFileDialog saveFileDialog =
                    new()
                    {
                        Filter = "Wave File|*.wav",
                        CheckPathExists = true,
                        AddExtension = true
                    };

                if (saveFileDialog.ShowDialog() != DialogResult.OK)
                {
                    return;
                }

                _outputFilePath = saveFileDialog.FileName;
                _waveWriter = new WaveFileWriter(_outputFilePath, _waveInEvent?.WaveFormat);
                _waveInEvent?.StartRecording();
                recordButton.Text = "Stop";
                _recording = true;
            }
            else
            {
                _waveInEvent?.StopRecording();
                recordButton.Text = "Record";
                _waveWriter?.Dispose();
                _waveWriter = null;
                _recording = false;
            }
        }

        private void OnPlaybackStopped(object? sender, StoppedEventArgs args)
        {
            _outputDevice?.Dispose();
            _outputDevice = null;
            _audioFile?.Dispose();
            _audioFile = null;
            if (_playRecorded)
            {
                playButton.Enabled = true;
            }
            else
            {
                playButton.Enabled = true;
            }

            playButton.Text = "Play Recorded";
        }

        private void OnPlayButtonClick(object sender, EventArgs e)
        {
            if (_outputDevice == null)
            {
                _outputDevice = new WaveOutEvent();
                _outputDevice.PlaybackStopped += OnPlaybackStopped;
            }

            if (_outputDevice.PlaybackState == PlaybackState.Playing)
            {
                _outputDevice.Stop();
            }

            _playRecorded = ((Button)sender).Name == "playButton";
            if (_playRecorded && File.Exists(_outputFilePath))
            {
                _audioFile = new AudioFileReader(_outputFilePath);
            }
            else if (!string.IsNullOrEmpty(_selectedFilePath))
            {
                _audioFile = new AudioFileReader(_selectedFilePath);
            }
            else
            {
                MessageBox.Show("Error", "Audio file not found");
                return;
            }

            _outputDevice.Init(_audioFile);

            if (_playRecorded)
            {
                playButton.Text = "Stop";
            }
            else
            {
                playButton.Text = "Play";
            }

            _outputDevice.Play();
        }
    }
}
