﻿namespace Multimedia.Controllers
{
    public partial class SharpenController : UserControl
    {
        private readonly Action<bool, int> _onTrackBarChange;
        private readonly Action<int> _onApplyClick;

        public SharpenController(Action<bool, int> onTrackBarChange, Action<int> onApplyClick)
        {
            InitializeComponent();
            _onTrackBarChange = onTrackBarChange;
            _onApplyClick = onApplyClick;
        }

        private void OnTrackBarScroll(object sender, EventArgs e)
        {
            _onTrackBarChange(previewCheckBox.Checked, trackBar.Value);
        }

        private void OnApplyButtonClick(object sender, EventArgs e)
        {
            _onApplyClick(trackBar.Value);
        }
    }
}
