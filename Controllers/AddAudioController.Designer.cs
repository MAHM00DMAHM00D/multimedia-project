﻿namespace Multimedia.Controllers
{
    partial class AddAudioController
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            recordButton = new Button();
            playButton = new Button();
            SuspendLayout();
            // 
            // recordButton
            // 
            recordButton.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            recordButton.Location = new Point(3, 3);
            recordButton.Name = "recordButton";
            recordButton.Size = new Size(251, 23);
            recordButton.TabIndex = 7;
            recordButton.Text = "Record";
            recordButton.UseVisualStyleBackColor = true;
            recordButton.Click += OnRecordButtonClick;
            // 
            // playButton
            // 
            playButton.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            playButton.Location = new Point(3, 32);
            playButton.Name = "playButton";
            playButton.Size = new Size(251, 23);
            playButton.TabIndex = 7;
            playButton.Text = "Play Recorded";
            playButton.UseVisualStyleBackColor = true;
            playButton.Visible = false;
            playButton.Click += OnPlayButtonClick;
            // 
            // AddAudioController
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            Controls.Add(playButton);
            Controls.Add(recordButton);
            Name = "AddAudioController";
            Size = new Size(257, 198);
            ResumeLayout(false);
        }

        #endregion

        private Button recordButton;
        private Button playButton;
    }
}
