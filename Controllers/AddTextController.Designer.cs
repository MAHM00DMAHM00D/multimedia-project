﻿namespace Multimedia.Controllers
{
    partial class AddTextController
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            label1 = new Label();
            applyButton = new Button();
            XUpDown = new NumericUpDown();
            label2 = new Label();
            YUpDown = new NumericUpDown();
            sizeUpDown = new NumericUpDown();
            label3 = new Label();
            label4 = new Label();
            colorBox = new ComboBox();
            textBox = new TextBox();
            label5 = new Label();
            previewCheckBox = new CheckBox();
            opacityUpDown = new NumericUpDown();
            label6 = new Label();
            ((System.ComponentModel.ISupportInitialize)XUpDown).BeginInit();
            ((System.ComponentModel.ISupportInitialize)YUpDown).BeginInit();
            ((System.ComponentModel.ISupportInitialize)sizeUpDown).BeginInit();
            ((System.ComponentModel.ISupportInitialize)opacityUpDown).BeginInit();
            SuspendLayout();
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new Point(27, 21);
            label1.Margin = new Padding(4, 0, 4, 0);
            label1.Name = "label1";
            label1.Size = new Size(14, 15);
            label1.TabIndex = 1;
            label1.Text = "X";
            // 
            // applyButton
            // 
            applyButton.BackColor = Color.FromArgb(33, 38, 44);
            applyButton.Dock = DockStyle.Bottom;
            applyButton.FlatAppearance.BorderSize = 0;
            applyButton.FlatAppearance.MouseDownBackColor = Color.FromArgb(122, 165, 210);
            applyButton.FlatStyle = FlatStyle.Flat;
            applyButton.Font = new Font("Segoe UI Semibold", 10F);
            applyButton.ForeColor = Color.White;
            applyButton.Location = new Point(0, 214);
            applyButton.Margin = new Padding(4, 5, 4, 5);
            applyButton.Name = "applyButton";
            applyButton.Padding = new Padding(12, 0, 0, 0);
            applyButton.Size = new Size(257, 46);
            applyButton.TabIndex = 7;
            applyButton.Text = "Apply";
            applyButton.UseVisualStyleBackColor = false;
            applyButton.Click += OnAplyButtonClick;
            // 
            // XUpDown
            // 
            XUpDown.Location = new Point(47, 18);
            XUpDown.Margin = new Padding(4, 3, 4, 3);
            XUpDown.Maximum = new decimal(new int[] { 360, 0, 0, 0 });
            XUpDown.Name = "XUpDown";
            XUpDown.Size = new Size(66, 23);
            XUpDown.TabIndex = 0;
            XUpDown.ValueChanged += XUpDown_ValueChanged;
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new Point(24, 61);
            label2.Margin = new Padding(4, 0, 4, 0);
            label2.Name = "label2";
            label2.Size = new Size(14, 15);
            label2.TabIndex = 1;
            label2.Text = "Y";
            // 
            // YUpDown
            // 
            YUpDown.Location = new Point(47, 59);
            YUpDown.Margin = new Padding(4, 3, 4, 3);
            YUpDown.Maximum = new decimal(new int[] { 360, 0, 0, 0 });
            YUpDown.Name = "YUpDown";
            YUpDown.Size = new Size(66, 23);
            YUpDown.TabIndex = 1;
            YUpDown.ValueChanged += OnYUpDownValueChanged;
            // 
            // sizeUpDown
            // 
            sizeUpDown.Location = new Point(176, 18);
            sizeUpDown.Margin = new Padding(4, 3, 4, 3);
            sizeUpDown.Maximum = new decimal(new int[] { 72, 0, 0, 0 });
            sizeUpDown.Name = "sizeUpDown";
            sizeUpDown.Size = new Size(66, 23);
            sizeUpDown.TabIndex = 2;
            sizeUpDown.Value = new decimal(new int[] { 32, 0, 0, 0 });
            sizeUpDown.ValueChanged += OnSizeUpDownValueChanged;
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new Point(135, 112);
            label3.Margin = new Padding(4, 0, 4, 0);
            label3.Name = "label3";
            label3.Size = new Size(36, 15);
            label3.TabIndex = 14;
            label3.Text = "Color";
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Location = new Point(135, 21);
            label4.Margin = new Padding(4, 0, 4, 0);
            label4.Name = "label4";
            label4.Size = new Size(27, 15);
            label4.TabIndex = 15;
            label4.Text = "Size";
            // 
            // colorBox
            // 
            colorBox.FormattingEnabled = true;
            colorBox.Items.AddRange(new object[] { "Black", "Blue", "Cyan", "Green", "Red", "White", "Yellow" });
            colorBox.Location = new Point(176, 108);
            colorBox.Margin = new Padding(4, 3, 4, 3);
            colorBox.Name = "colorBox";
            colorBox.Size = new Size(66, 23);
            colorBox.Sorted = true;
            colorBox.TabIndex = 4;
            colorBox.SelectedIndexChanged += OnColorBoxSelectedIndexChanged;
            // 
            // textBox
            // 
            textBox.Font = new Font("Tahoma", 10F);
            textBox.Location = new Point(15, 178);
            textBox.Margin = new Padding(4, 3, 4, 3);
            textBox.Name = "textBox";
            textBox.Size = new Size(229, 24);
            textBox.TabIndex = 6;
            textBox.TextChanged += OnTextBoxTextChanged;
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Location = new Point(12, 159);
            label5.Margin = new Padding(4, 0, 4, 0);
            label5.Name = "label5";
            label5.Size = new Size(28, 15);
            label5.TabIndex = 14;
            label5.Text = "Text";
            // 
            // previewCheckBox
            // 
            previewCheckBox.AutoSize = true;
            previewCheckBox.Location = new Point(28, 113);
            previewCheckBox.Margin = new Padding(4, 3, 4, 3);
            previewCheckBox.Name = "previewCheckBox";
            previewCheckBox.Size = new Size(67, 19);
            previewCheckBox.TabIndex = 5;
            previewCheckBox.Text = "Preview";
            previewCheckBox.UseVisualStyleBackColor = true;
            // 
            // opacityUpDown
            // 
            opacityUpDown.Location = new Point(176, 59);
            opacityUpDown.Margin = new Padding(4, 3, 4, 3);
            opacityUpDown.Maximum = new decimal(new int[] { 255, 0, 0, 0 });
            opacityUpDown.Name = "opacityUpDown";
            opacityUpDown.Size = new Size(67, 23);
            opacityUpDown.TabIndex = 3;
            opacityUpDown.Value = new decimal(new int[] { 255, 0, 0, 0 });
            opacityUpDown.ValueChanged += OnAlphaUpDownValueChanged;
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.Location = new Point(135, 61);
            label6.Margin = new Padding(4, 0, 4, 0);
            label6.Name = "label6";
            label6.Size = new Size(38, 15);
            label6.TabIndex = 1;
            label6.Text = "Alpha";
            // 
            // AddTextController
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            Controls.Add(opacityUpDown);
            Controls.Add(previewCheckBox);
            Controls.Add(textBox);
            Controls.Add(colorBox);
            Controls.Add(sizeUpDown);
            Controls.Add(label5);
            Controls.Add(label3);
            Controls.Add(label4);
            Controls.Add(YUpDown);
            Controls.Add(XUpDown);
            Controls.Add(applyButton);
            Controls.Add(label6);
            Controls.Add(label2);
            Controls.Add(label1);
            Margin = new Padding(4, 3, 4, 3);
            Name = "AddTextController";
            Size = new Size(257, 260);
            ((System.ComponentModel.ISupportInitialize)XUpDown).EndInit();
            ((System.ComponentModel.ISupportInitialize)YUpDown).EndInit();
            ((System.ComponentModel.ISupportInitialize)sizeUpDown).EndInit();
            ((System.ComponentModel.ISupportInitialize)opacityUpDown).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button applyButton;
        private System.Windows.Forms.NumericUpDown XUpDown;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown YUpDown;
        private System.Windows.Forms.NumericUpDown sizeUpDown;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox colorBox;
        private System.Windows.Forms.TextBox textBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox previewCheckBox;
        private System.Windows.Forms.NumericUpDown opacityUpDown;
        private System.Windows.Forms.Label label6;
    }
}
