﻿namespace Multimedia.Controllers
{
    public partial class AddTextController : UserControl
    {
        private readonly Action<TextArguments>? _onApplyButtonClick;
        private readonly Action<TextArguments>? _onTextChange;
        private readonly Action<TextArguments>? _onLocationChange;
        private readonly Action<TextArguments>? _onSizeChange;
        private readonly Action<TextArguments>? _onColorChange;
        private readonly Action<TextArguments>? _onAlphaChange;

        public AddTextController(
            int width,
            int height,
            Action<TextArguments>? onApplyButtonClick = null,
            Action<TextArguments>? onTextChange = null,
            Action<TextArguments>? onLocationChange = null,
            Action<TextArguments>? onSizeChange = null,
            Action<TextArguments>? onColorChange = null,
            Action<TextArguments>? onAlphaChange = null
        )
        {
            InitializeComponent();
            XUpDown.Maximum = width;
            YUpDown.Maximum = height;
            colorBox.SelectedIndex = 0;
            _onApplyButtonClick = onApplyButtonClick;
            _onTextChange = onTextChange;
            _onLocationChange = onLocationChange;
            _onSizeChange = onSizeChange;
            _onColorChange = onColorChange;
            _onAlphaChange = onAlphaChange;
        }

        private void OnAplyButtonClick(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(textBox.Text))
            {
                MessageBox.Show("Insert Text Before");
                return;
            }

            if (_onApplyButtonClick is not null)
            {
                _onApplyButtonClick(
                    new TextArguments(
                        (int)XUpDown.Value,
                        (int)YUpDown.Value,
                        textBox.Text,
                        (int)sizeUpDown.Value,
                        Color.FromName(colorBox.Text),
                        (int)opacityUpDown.Value,
                        previewCheckBox.Checked
                    )
                );
            }
        }

        private void XUpDown_ValueChanged(object sender, EventArgs e)
        {
            if (_onLocationChange is not null)
            {
                _onLocationChange(
                    new TextArguments(
                        (int)XUpDown.Value,
                        (int)YUpDown.Value,
                        textBox.Text,
                        (int)sizeUpDown.Value,
                        Color.FromName(colorBox.Text),
                        (int)opacityUpDown.Value,
                        previewCheckBox.Checked
                    )
                );
            }
        }

        private void OnYUpDownValueChanged(object sender, EventArgs e)
        {
            if (_onLocationChange is not null)
            {
                _onLocationChange(
                    new TextArguments(
                        (int)XUpDown.Value,
                        (int)YUpDown.Value,
                        textBox.Text,
                        (int)sizeUpDown.Value,
                        Color.FromName(colorBox.Text),
                        (int)opacityUpDown.Value,
                        previewCheckBox.Checked
                    )
                );
            }
        }

        private void OnSizeUpDownValueChanged(object sender, EventArgs e)
        {
            if (_onSizeChange is not null)
            {
                _onSizeChange(
                    new TextArguments(
                        (int)XUpDown.Value,
                        (int)YUpDown.Value,
                        textBox.Text,
                        (int)sizeUpDown.Value,
                        Color.FromName(colorBox.Text),
                        (int)opacityUpDown.Value,
                        previewCheckBox.Checked
                    )
                );
            }
        }

        private void OnColorBoxSelectedIndexChanged(object sender, EventArgs e)
        {
            if (_onColorChange is not null)
            {
                _onColorChange(
                    new TextArguments(
                        (int)XUpDown.Value,
                        (int)YUpDown.Value,
                        textBox.Text,
                        (int)sizeUpDown.Value,
                        Color.FromName(colorBox.Text),
                        (int)opacityUpDown.Value,
                        previewCheckBox.Checked
                    )
                );
            }
        }

        private void OnTextBoxTextChanged(object sender, EventArgs e)
        {
            if (_onTextChange is not null)
            {
                _onTextChange(
                    new TextArguments(
                        (int)XUpDown.Value,
                        (int)YUpDown.Value,
                        textBox.Text,
                        (int)sizeUpDown.Value,
                        Color.FromName(colorBox.Text),
                        (int)opacityUpDown.Value,
                        previewCheckBox.Checked
                    )
                );
            }
        }

        private void OnAlphaUpDownValueChanged(object sender, EventArgs e)
        {
            if (_onAlphaChange is not null)
            {
                _onAlphaChange(
                    new TextArguments(
                        (int)XUpDown.Value,
                        (int)YUpDown.Value,
                        textBox.Text,
                        (int)sizeUpDown.Value,
                        Color.FromName(colorBox.Text),
                        (int)opacityUpDown.Value,
                        previewCheckBox.Checked
                    )
                );
            }
        }

        public void OnSelectTextPositionByMouse(Point point)
        {
            if (XUpDown.Maximum < point.X)
            {
                point.X = (int)XUpDown.Maximum;
            }

            if (point.X < 0)
            {
                point.X = 0;
            }

            if (YUpDown.Maximum < point.Y)
            {
                point.Y = (int)YUpDown.Maximum;
            }

            if (point.Y < 0)
            {
                point.Y = 0;
            }

            XUpDown.Value = point.X;
            YUpDown.Value = point.Y;
            if (_onLocationChange is not null)
            {
                _onLocationChange(
                    new TextArguments(
                        (int)XUpDown.Value,
                        (int)YUpDown.Value,
                        textBox.Text,
                        (int)sizeUpDown.Value,
                        Color.FromName(colorBox.Text),
                        (int)opacityUpDown.Value,
                        previewCheckBox.Checked
                    )
                );
            }
        }
    }

    public record TextArguments(
        int X,
        int Y,
        string Text,
        int Size,
        Color Color,
        int Alpha,
        bool Preview
    );
}
