﻿using Multimedia.Helpers;

namespace Multimedia.Controllers
{
    public partial class BlendingController : UserControl
    {
        private Color? _color;
        private readonly Action<BendingArguments> _onApplyBendingClick;
        private readonly Action<SelectionMode> _onSelectionModeChange;
        private readonly List<Color[]> _colorMaps = [];

        public BlendingController(
            Action<BendingArguments> onApplyBendingClick,
            Action<SelectionMode> onSelectionModeChange
        )
        {
            _onApplyBendingClick = onApplyBendingClick;
            _onSelectionModeChange = onSelectionModeChange;
            InitializeComponent();
            ColorMaps.AddColorMaps(_colorMaps);
        }

        private void OnApplyBlendingButtonClick(object sender, EventArgs e)
        {
            Color[] colorMap;
            if (colorMapComboBox.SelectedIndex == 5)
            {
                if (_color is null)
                {
                    MessageBox.Show(
                        "Select Color First",
                        "Alert",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Warning
                    );
                    return;
                }

                colorMap = ColorMaps.GetColorMap((Color)_color);
            }
            else
            {
                colorMap = _colorMaps[colorMapComboBox.SelectedIndex];
            }

            Enum.TryParse(colorMapComboBox.Text, out SelectionMode selectionMode);
            _onApplyBendingClick(new BendingArguments(colorMap, selectionMode));
        }

        private void OnColorMapComboBoxSelectedIndexChanged(object sender, EventArgs e)
        {
            selectedColorLabel.Visible = colorMapComboBox.SelectedIndex == 5;
            selectColorButton.Visible = colorMapComboBox.SelectedIndex == 5;
        }

        private void SelectionModeComboBoxSelectedIndexChanged(object sender, EventArgs e)
        {
            Enum.TryParse(selectionModeComboBox.Text, out SelectionMode selectionMode);
            _onSelectionModeChange(selectionMode);
        }
    }

    public record BendingArguments(Color[] ColorMap, SelectionMode SelectionMode);
}
