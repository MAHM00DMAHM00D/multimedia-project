﻿namespace Multimedia.Controllers
{
    public partial class CropController : UserControl
    {
        private readonly Action _onApplyCropClick;

        public CropController(Action onApplyCropClick)
        {
            _onApplyCropClick = onApplyCropClick;
            InitializeComponent();
        }

        private void OnApplyBlendingButtonClick(object sender, EventArgs e)
        {
            _onApplyCropClick();
        }
    }

    public record CropArguments(Color[] ColorMap, SelectionMode SelectionMode);
}
