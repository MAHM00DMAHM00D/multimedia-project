﻿namespace Multimedia.Controllers
{
    partial class SharpenController
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            applyButton = new Button();
            trackBar = new TrackBar();
            previewCheckBox = new CheckBox();
            ((System.ComponentModel.ISupportInitialize)trackBar).BeginInit();
            SuspendLayout();
            // 
            // applyButton
            // 
            applyButton.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
            applyButton.Location = new Point(3, 124);
            applyButton.Name = "applyButton";
            applyButton.Size = new Size(251, 23);
            applyButton.TabIndex = 0;
            applyButton.Text = "Apply Sharpen";
            applyButton.UseVisualStyleBackColor = true;
            applyButton.Click += OnApplyButtonClick;
            // 
            // trackBar
            // 
            trackBar.Location = new Point(3, 3);
            trackBar.Maximum = 20;
            trackBar.Minimum = 1;
            trackBar.Name = "trackBar";
            trackBar.Size = new Size(251, 45);
            trackBar.TabIndex = 1;
            trackBar.Value = 1;
            trackBar.Scroll += OnTrackBarScroll;
            // 
            // previewCheckBox
            // 
            previewCheckBox.AutoSize = true;
            previewCheckBox.Location = new Point(3, 43);
            previewCheckBox.Name = "previewCheckBox";
            previewCheckBox.Size = new Size(67, 19);
            previewCheckBox.TabIndex = 2;
            previewCheckBox.Text = "Preview";
            previewCheckBox.UseVisualStyleBackColor = true;
            // 
            // SharpenController
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            Controls.Add(previewCheckBox);
            Controls.Add(trackBar);
            Controls.Add(applyButton);
            Name = "SharpenController";
            Size = new Size(257, 150);
            ((System.ComponentModel.ISupportInitialize)trackBar).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Button applyButton;
        private TrackBar trackBar;
        private CheckBox previewCheckBox;
    }
}
