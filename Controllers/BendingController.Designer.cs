﻿namespace Multimedia.Controllers
{
    partial class BlendingController
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            selectColorButton = new Button();
            selectedColorLabel = new Label();
            colorMapComboBox = new ComboBox();
            applyBlendingButton = new Button();
            selectionModeComboBox = new ComboBox();
            SuspendLayout();
            // 
            // selectColorButton
            // 
            selectColorButton.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            selectColorButton.Location = new Point(3, 89);
            selectColorButton.Name = "selectColorButton";
            selectColorButton.Size = new Size(251, 23);
            selectColorButton.TabIndex = 7;
            selectColorButton.Text = "Select Color";
            selectColorButton.UseVisualStyleBackColor = true;
            selectColorButton.Visible = false;
            // 
            // selectedColorLabel
            // 
            selectedColorLabel.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            selectedColorLabel.BorderStyle = BorderStyle.FixedSingle;
            selectedColorLabel.Location = new Point(3, 63);
            selectedColorLabel.Name = "selectedColorLabel";
            selectedColorLabel.Size = new Size(251, 23);
            selectedColorLabel.TabIndex = 8;
            selectedColorLabel.Visible = false;
            // 
            // colorMapComboBox
            // 
            colorMapComboBox.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            colorMapComboBox.FormattingEnabled = true;
            colorMapComboBox.Items.AddRange(new object[] { "Cool Warm", "Parula", "Heat", "Purple Blue", "Rainbow", "Custom Color" });
            colorMapComboBox.Location = new Point(3, 37);
            colorMapComboBox.Name = "colorMapComboBox";
            colorMapComboBox.Size = new Size(251, 23);
            colorMapComboBox.TabIndex = 9;
            colorMapComboBox.Text = "Cool Warm";
            colorMapComboBox.SelectedIndexChanged += OnColorMapComboBoxSelectedIndexChanged;
            // 
            // applyBlendingButton
            // 
            applyBlendingButton.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            applyBlendingButton.Location = new Point(3, 172);
            applyBlendingButton.Name = "applyBlendingButton";
            applyBlendingButton.Size = new Size(251, 23);
            applyBlendingButton.TabIndex = 10;
            applyBlendingButton.Text = "Apply Blending";
            applyBlendingButton.UseVisualStyleBackColor = true;
            applyBlendingButton.Click += OnApplyBlendingButtonClick;
            // 
            // selectionModeComboBox
            // 
            selectionModeComboBox.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            selectionModeComboBox.FormattingEnabled = true;
            selectionModeComboBox.Items.AddRange(new object[] { "Rectangle", "Ellipse", "Polygon" });
            selectionModeComboBox.Location = new Point(3, 8);
            selectionModeComboBox.Name = "selectionModeComboBox";
            selectionModeComboBox.Size = new Size(251, 23);
            selectionModeComboBox.TabIndex = 9;
            selectionModeComboBox.Text = "Rectanlge";
            selectionModeComboBox.SelectedIndexChanged += SelectionModeComboBoxSelectedIndexChanged;
            // 
            // BendingController
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            Controls.Add(applyBlendingButton);
            Controls.Add(selectColorButton);
            Controls.Add(selectedColorLabel);
            Controls.Add(selectionModeComboBox);
            Controls.Add(colorMapComboBox);
            Name = "BendingController";
            Size = new Size(257, 198);
            ResumeLayout(false);
        }

        #endregion

        private Button selectColorButton;
        private Label selectedColorLabel;
        private ComboBox colorMapComboBox;
        private Button applyBlendingButton;
        private ComboBox selectionModeComboBox;
    }
}
