﻿using QuestPDF.Fluent;
using QuestPDF.Infrastructure;

namespace Multimedia.Helpers;

public class PDFGenerator
{
    public static Document GeneratePDFReport(
        Bitmap image,
        string name,
        string progress,
        string classification,
        string details
    )
    {
        var tempFileName = Path.GetTempFileName();
        image.Save(tempFileName);

        return Document.Create(d =>
        {
            d.Page(page =>
            {
                page.Margin(2, Unit.Centimetre);
                page.Header()
                    .PaddingBottom(1, Unit.Centimetre)
                    .Text(name)
                    .AlignLeft()
                    .FontSize(32)
                    .Bold();
                page.Content()
                    .Column(c =>
                    {
                        c.Spacing(1, Unit.Centimetre);
                        c.Item().Image(tempFileName).FitWidth();
                        c.Item().Text($"Progress: {progress}");
                        c.Item().Text($"Classification: {classification}");
                        if (!string.IsNullOrWhiteSpace(details))
                        {
                            c.Item().Text($"Mode Details:\n{details}").Justify();
                        }
                    });
                page.Footer().Text(DateTime.Now.ToString()).AlignCenter();
            });
        });
    }
}
