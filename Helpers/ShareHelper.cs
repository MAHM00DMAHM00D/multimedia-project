﻿using System.Diagnostics;
using System.Drawing.Imaging;
using System.Net.Http.Json;
using System.Text.Json.Serialization;
using System.Web;

namespace Multimedia.Helpers;

public class ShareHelper
{
    private static readonly string WhatsappTemplate = "https://api.whatsapp.com/send?text={text}";

    public static string WhatsappUrl(string text)
    {
        return HttpUtility.UrlPathEncode(WhatsappTemplate.Replace("{text}", text));
    }

    public static void OpenFileUsingDefaultProgram(string filePath)
    {
        var process = new Process
        {
            StartInfo = new ProcessStartInfo(filePath) { UseShellExecute = true }
        };

        process.Start();
        process.WaitForExit();
    }

    public static async Task<Response?> ShareImage(Bitmap image)
    {
        var tempFileName = Path.GetTempFileName() + ".webp";
#pragma warning disable CA1416 // Validate platform compatibility
        image.Save(tempFileName, ImageFormat.Webp);
#pragma warning restore CA1416 // Validate platform compatibility
        return await UploadFile(tempFileName);
    }

    public static async Task<Response?> SharePDFFile(string filePath)
    {
        return await UploadFile(filePath);
    }

    public static async Task<Response?> ShareZipFile(string filePath)
    {
        return await UploadFile(filePath);
    }

    private static async Task<Response?> UploadFile(string filePath)
    {
        using var httpClient = new HttpClient();
        using var form = new MultipartFormDataContent();
        using var fileStream = File.OpenRead(filePath);
        form.Add(new StreamContent(fileStream), "file", Path.GetFileName(filePath));

        using var response = await httpClient.PostAsync("https://file.io/", form);
        response.EnsureSuccessStatusCode();
        return await response.Content.ReadFromJsonAsync<Response>();
    }

    public partial class Response
    {
        [JsonPropertyName("success")]
        public bool Success { get; set; }

        [JsonPropertyName("link")]
        public string Link { get; set; }

        [JsonPropertyName("expires")]
        public DateTimeOffset Expires { get; set; }
    }
}
